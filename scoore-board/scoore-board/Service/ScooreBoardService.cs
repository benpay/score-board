﻿using scoore_board.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace scoore_board
{
    public class ScooreBoardService
    {
        private List<ScooreInfoModel> resultOfMatchs;

        public ScooreBoardService()
        {
            resultOfMatchs = new List<ScooreInfoModel>();
        }

        public void StartGame(string homeTeam, string awayTeam)
        {
            resultOfMatchs.Add(new ScooreInfoModel
            {
                teamsInfoModel = new TeamsInfoModel[]
                {
                    new TeamsInfoModel
                    {
                        name = homeTeam,
                        scoore = 0
                    },
                    new TeamsInfoModel
                    {
                        name = awayTeam,
                        scoore = 0
                    }
                },
                dateStart = DateTime.Now
            });
        }

        public bool FinishGame(string homeTeam, string awayTeam)
        {
            var isFinished = false;
            var matchFinished = resultOfMatchs.FirstOrDefault(x => x.teamsInfoModel.FirstOrDefault().name == homeTeam && x.teamsInfoModel.LastOrDefault().name == awayTeam);
            if (matchFinished != null)
            {
                isFinished = resultOfMatchs.Remove(matchFinished);
            }
            return isFinished;
        }

        public bool UpdateScore(string homeTeam, string awayTeam, int homeScore, int awatScore)
        {
            var updated = false;
            var matchFinished = resultOfMatchs.FirstOrDefault(x => x.teamsInfoModel.FirstOrDefault().name == homeTeam && x.teamsInfoModel.LastOrDefault().name == awayTeam);

            if (matchFinished != null)
            {
                matchFinished.teamsInfoModel.FirstOrDefault().scoore = homeScore;
                matchFinished.teamsInfoModel.LastOrDefault().scoore = awatScore;
                updated = true;
            }
            return updated;
        }

        public List<ScooreInfoModel> SummaryMatchs()
        {
            var matchsOrdererByDate = resultOfMatchs.OrderByDescending(x => x.dateStart);
            return matchsOrdererByDate.OrderByDescending(y => y.teamsInfoModel.Sum(z => z.scoore)).ToList();
        }
    }
}
