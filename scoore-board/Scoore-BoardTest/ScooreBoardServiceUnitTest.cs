using Moq;
using scoore_board;
using ScoreBoardUnitTest;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Scoore_BoardTest
{
    public class ScooreBoardServiceUnitTest
    {
        [Fact]
        public void StartGameShouldAddAPairOfTeams()
        {
            var scooreBoard = new ScooreBoardService();

            var mockData = SeedTeams.ListOfMatchs().FirstOrDefault();
            scooreBoard.StartGame(mockData.teamsInfoModel.FirstOrDefault().name, mockData.teamsInfoModel.LastOrDefault().name);
            var actual = scooreBoard.SummaryMatchs().First(x => x.teamsInfoModel.FirstOrDefault().name == mockData.teamsInfoModel.FirstOrDefault().name
                                                             && x.teamsInfoModel.LastOrDefault().name == mockData.teamsInfoModel.LastOrDefault().name);

            Assert.Equal(mockData.teamsInfoModel.FirstOrDefault().name, actual.teamsInfoModel.FirstOrDefault().name);
            Assert.Equal(mockData.teamsInfoModel.LastOrDefault().name, actual.teamsInfoModel.LastOrDefault().name);
        }

        [Fact]
        public void StartGameShouldSetDefaultScoore()
        {
            var scooreBoard = new ScooreBoardService();
            var expectedScore = 0;

            var mockData = SeedTeams.ListOfMatchs().FirstOrDefault();
            scooreBoard.StartGame(mockData.teamsInfoModel.FirstOrDefault().name, mockData.teamsInfoModel.LastOrDefault().name);
            var actual = scooreBoard.SummaryMatchs().First(x => x.teamsInfoModel.FirstOrDefault().name == mockData.teamsInfoModel.FirstOrDefault().name
                                                             && x.teamsInfoModel.LastOrDefault().name == mockData.teamsInfoModel.LastOrDefault().name);
            var actualScore = actual.teamsInfoModel.FirstOrDefault().scoore + actual.teamsInfoModel.LastOrDefault().scoore;

            Assert.Equal(expectedScore, actualScore);
        }

        [Fact]
        public void FinishGameShouldRemoveTeamWithAPairOfTeams()
        {
            var scooreBoard = new ScooreBoardService();

            var mockData = SeedTeams.ListOfMatchs().FirstOrDefault();
            scooreBoard.StartGame(mockData.teamsInfoModel.FirstOrDefault().name, mockData.teamsInfoModel.LastOrDefault().name);
            var actual = scooreBoard.FinishGame(mockData.teamsInfoModel.FirstOrDefault().name, mockData.teamsInfoModel.LastOrDefault().name);

            Assert.True(actual);
        }

        [Fact]
        public void FinishGameShouldRemoveAPairOfTeamsWithSeveralTeams()
        {
            var scooreBoard = new ScooreBoardService();
            var listMatchs = SeedTeams.ListOfMatchs();

            InitializeBoardWithMatchs(scooreBoard, listMatchs);
            scooreBoard.FinishGame(listMatchs.FirstOrDefault().teamsInfoModel.FirstOrDefault().name, listMatchs.FirstOrDefault().teamsInfoModel.LastOrDefault().name);

            var actual = scooreBoard.SummaryMatchs();

            Assert.Equal(listMatchs.Count() - 1, actual.Count());
        }

        [Fact]
        public void FinishGameShouldRemoveAPairOfTeamsWithSeveralTeamsCheckingSpecificPairOfTeamsRemoved()
        {
            var scooreBoard = new ScooreBoardService();
            var listMatchs = SeedTeams.ListOfMatchs();
            var home = listMatchs.FirstOrDefault().teamsInfoModel.FirstOrDefault().name;
            var away = listMatchs.FirstOrDefault().teamsInfoModel.LastOrDefault().name;

            InitializeBoardWithMatchs(scooreBoard, listMatchs);
            scooreBoard.FinishGame(home, away);

            var actual = scooreBoard.SummaryMatchs().Find(x => x.teamsInfoModel.FirstOrDefault().name == home && x.teamsInfoModel.LastOrDefault().name == away);

            Assert.Null(actual);
        }

        [Fact]
        public void FinishGameWithWrongValuesShouldReturnFalse()
        {
            var scooreBoard = new ScooreBoardService();
            var listMatchs = SeedTeams.ListOfMatchs();
            InitializeBoardWithMatchs(scooreBoard, listMatchs);
            var actual = scooreBoard.FinishGame(It.IsAny<string>(), It.IsAny<string>());

            Assert.False(actual);
        }

        [Fact]
        public void UpdateGameShouldUpdateScooresForAPairOfTeams()
        {
            var scooreBoard = new ScooreBoardService();

            var mockData = SeedTeams.ListOfMatchs().FirstOrDefault();
            var home = mockData.teamsInfoModel.FirstOrDefault().name;
            var away = mockData.teamsInfoModel.LastOrDefault().name;
            scooreBoard.StartGame(home, away);

            var actualResult = scooreBoard.UpdateScore(home, away, 1, 5);
            var actualScoore = scooreBoard.SummaryMatchs().FirstOrDefault().teamsInfoModel.Sum(x => x.scoore);

            Assert.Equal(6, actualScoore);
            Assert.True(actualResult);
        }

        [Fact]
        public void UpdateGameShouldUpdateScooreForAPairTeamsWithSeveralTeams()
        {
            var expectedScoores = new List<int>() { 5, 12, 4, 12, 4 };
            var expectedResult = new List<bool>();
            var scooreBoard = new ScooreBoardService();
            var listMatchs = SeedTeams.ListOfMatchs();
            InitializeBoardWithMatchs(scooreBoard, listMatchs);

            foreach (var match in listMatchs)
            {
                expectedResult.Add(scooreBoard.UpdateScore(match.teamsInfoModel.FirstOrDefault().name, match.teamsInfoModel.LastOrDefault().name, match.teamsInfoModel.FirstOrDefault().scoore, match.teamsInfoModel.LastOrDefault().scoore));
            }

            var matchsUpdated = scooreBoard.SummaryMatchs();
            var i = 0;
            foreach (var match in matchsUpdated)
            {
                Assert.Equal(expectedScoores[i], match.teamsInfoModel.Sum(x => x.scoore));
                Assert.True(expectedResult[i]);
                i++;
            }
        }

        [Fact]
        public void UpdateGameWithWrongValuesShouldReturnFalse()
        {
            var scooreBoard = new ScooreBoardService();
            var listMatchs = SeedTeams.ListOfMatchs();
            InitializeBoardWithMatchs(scooreBoard, listMatchs);

            var actualResult = scooreBoard.UpdateScore(It.IsAny<string>(), It.IsAny<string>(), 3, 3);

            Assert.False(actualResult);
        }

        [Fact]
        public void GetSummaryShouldReturnResultsOrdered()
        {
            var scooreBoard = new ScooreBoardService();
            var listMatchs = SeedTeams.ListOfMatchs();
            var ordererTeams = SeedTeams.OrdererTeams();
            InitializeBoardWithMatchs(scooreBoard, listMatchs);

            foreach (var match in listMatchs)
            {
                scooreBoard.UpdateScore(match.teamsInfoModel.FirstOrDefault().name, match.teamsInfoModel.LastOrDefault().name, match.teamsInfoModel.FirstOrDefault().scoore, match.teamsInfoModel.LastOrDefault().scoore);
            }

            var actualMatchs = scooreBoard.SummaryMatchs();
            int i = 0;
            foreach(var actualMatch in actualMatchs)
            {
                Assert.Equal(ordererTeams[i].teamsInfoModel.FirstOrDefault().name, actualMatch.teamsInfoModel.FirstOrDefault().name);
                Assert.Equal(ordererTeams[i].teamsInfoModel.LastOrDefault().name, actualMatch.teamsInfoModel.LastOrDefault().name);
                i++;
            }
        }


        private static void InitializeBoardWithMatchs(ScooreBoardService scooreBoard, List<scoore_board.Model.ScooreInfoModel> listMatchs)
        {
            foreach (var match in listMatchs)
            {
                scooreBoard.StartGame(match.teamsInfoModel.FirstOrDefault().name, match.teamsInfoModel.LastOrDefault().name);
            }
        }
    }
}
