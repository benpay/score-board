﻿using scoore_board.Model;
using System.Collections.Generic;

namespace ScoreBoardUnitTest
{
    public class SeedTeams
    {
        public static List<ScooreInfoModel> ListOfMatchs()
        {
            return new List<ScooreInfoModel>
            {
                new ScooreInfoModel
                {
                    teamsInfoModel = new TeamsInfoModel[]
                    {
                        new TeamsInfoModel
                        {
                            name = "Mexico",
                            scoore = 0
                        },
                        new TeamsInfoModel
                        {
                            name = "Canada",
                            scoore = 5
                        }
                    }
                },
                new ScooreInfoModel
                {
                    teamsInfoModel = new TeamsInfoModel[]
                    {
                        new TeamsInfoModel
                        {
                            name = "Spain",
                            scoore = 10
                        },
                        new TeamsInfoModel
                        {
                            name = "Brazil",
                            scoore = 2
                        }
                    }
                },
                new ScooreInfoModel
                {
                    teamsInfoModel = new TeamsInfoModel[]
                    {
                        new TeamsInfoModel
                        {
                            name = "Germany",
                            scoore = 2
                        },
                        new TeamsInfoModel
                        {
                            name = "France",
                            scoore = 2
                        }
                    }
                },
                new ScooreInfoModel
                {
                    teamsInfoModel = new TeamsInfoModel[]
                    {
                        new TeamsInfoModel
                        {
                            name = "Uruguay",
                            scoore = 6
                        },
                        new TeamsInfoModel
                        {
                            name = "Italy",
                            scoore = 6
                        }
                    }
                },
                new ScooreInfoModel
                {
                    teamsInfoModel = new TeamsInfoModel[]
                    {
                        new TeamsInfoModel
                        {
                            name = "Argentina",
                            scoore = 3
                        },
                        new TeamsInfoModel
                        {
                            name = "Australia",
                            scoore = 1
                        }
                    }
                }
            };
        }


        public static List<ScooreInfoModel> OrdererTeams()
        {
            return new List<ScooreInfoModel>
            {
                new ScooreInfoModel
                {
                    teamsInfoModel = new TeamsInfoModel[]
                    {
                        new TeamsInfoModel
                        {
                            name = "Uruguay",
                            scoore = 6
                        },
                        new TeamsInfoModel
                        {
                            name = "Italy",
                            scoore = 6
                        }
                    }
                },
                new ScooreInfoModel
                {
                    teamsInfoModel = new TeamsInfoModel[]
                    {
                        new TeamsInfoModel
                        {
                            name = "Spain",
                            scoore = 10
                        },
                        new TeamsInfoModel
                        {
                            name = "Brazil",
                            scoore = 2
                        }
                    }
                },
                new ScooreInfoModel
                {
                    teamsInfoModel = new TeamsInfoModel[]
                    {
                        new TeamsInfoModel
                        {
                            name = "Mexico",
                            scoore = 0
                        },
                        new TeamsInfoModel
                        {
                            name = "Canada",
                            scoore = 5
                        }
                    }
                },
                new ScooreInfoModel
                {
                    teamsInfoModel = new TeamsInfoModel[]
                    {
                        new TeamsInfoModel
                        {
                            name = "Argentina",
                            scoore = 3
                        },
                        new TeamsInfoModel
                        {
                            name = "Australia",
                            scoore = 1
                        }
                    }
                },
                new ScooreInfoModel
                {
                    teamsInfoModel = new TeamsInfoModel[]
                    {
                        new TeamsInfoModel
                        {
                            name = "Germany",
                            scoore = 2
                        },
                        new TeamsInfoModel
                        {
                            name = "France",
                            scoore = 2
                        }
                    }
                }
            };
        }
    }
}